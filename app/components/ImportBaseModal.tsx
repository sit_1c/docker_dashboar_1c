import * as React from 'react'
import Modal from './modal'
import * as classNames from 'classnames'

interface ModalProperties {
    id: string
    basename: string
    onAction?: (username: string, password: string) => void
}

interface ModalState {
    file: File
    username: string
    password: string
    saved: boolean
}

const SLICE_SIZE = 1000 * 1024

export class ImportBaseModal extends React.Component<ModalProperties, ModalState> {

    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            file: null,
            username: '',
            password: '',
            saved: false
        }
    }

    doAction(modalForm:JQuery<HTMLElement>) {
        if( this.state.file ) {
            var xhr = new XMLHttpRequest;
                        
            xhr.open('PUT', `http://${this.context.ServerInfo.dockerHost}:${this.context.ServerInfo.httpPort}/tmp/${this.props.basename}.dt`, true)
            xhr.setRequestHeader('crossDomain', 'true')
            xhr.overrideMimeType('application/octet-stream');

            // Listen to the upload progress.
            var progressBar = document.getElementById(this.props.id + "progress") as HTMLProgressElement;
            xhr.upload.addEventListener('progress', (evn:ProgressEvent<XMLHttpRequestUpload>) => {
                //console.log('upload progress ' + e.loaded  + ' of ' + e.total)
                if (evn.lengthComputable) {
                    progressBar.value = (evn.loaded / evn.total) * 100;
                    //progressBar.textContent = progressBar.value.toString(); // Fallback for unsupported browsers.
                }
            });

            xhr.upload.addEventListener('loadend', (evn:any) => {
                console.log('uploading completed')
                this.props.onAction(this.state.username, this.state.password)

                //Close manuale
                let divElem = document.getElementById(this.props.id)
                divElem.dispatchEvent(new CustomEvent('saveCredentails'))
                
                modalForm.modal('hide')
            })

            xhr.upload.addEventListener('error', (evn:any) => {
                console.log('error:' + evn)
            })

            xhr.upload.addEventListener('abort', (evn:any) => {
                console.log('aborted')
            })

            xhr.send(this.state.file)
        }

        return false
    }

    onLoadCredentials(e:any) {
        console.log('onLoadCredentials called', e)
        
        this.setState({
            username: (e.username ?e.username :this.state.username),
            password: (e.username ?e.password :this.state.password),
            saved: (e.saved ?e.saved :false)
        })
    }

    onSaveCredentials(e:any) {
        console.log('onSaveCredentials called')
        
        return (this.state.saved ?this.state :undefined)
    }

    handleUsernameChange(e:any) {
        this.setState({
            username: e.target.value
        });
    }

    handlePasswordChange(e:any) {
        this.setState({
            password: e.target.value
        });
    }

    handleFilenameChange(e:any) {
        this.setState({
            file: e.target.files[0]
        });
    }

    handleSave(e:any) {
        this.setState({
          saved: e.target.checked
        });
    }

    render() {
        let inputClass = classNames({
            "form-group": true,
            "has-error": false
        })
        
        return (
            <Modal id={this.props.id} buttonText="Logon" title="Login information for infobase" 
                    onButtonClicked={this.doAction.bind(this)}
                    storeId={this.props.basename}
                    onLoadCredentials={this.onLoadCredentials.bind(this)}
                    onSaveCredentials={this.onSaveCredentials.bind(this)}>
                <form className="form-horizontal">
                    <div className={inputClass}>
                        <label htmlFor={this.props.id + "username"} className="col-sm-3 control-label">User name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                onChange={this.handleUsernameChange.bind(this)}
                                placeholder="e.g Administrator"/>
                        </div>
                        
                        <label htmlFor={this.props.id + "password"} className="col-sm-3 control-label">User password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                className="form-control"
                                onChange={this.handlePasswordChange.bind(this)}
                                placeholder="e.g password for Administrator"/>
                        </div>

                        <label htmlFor={this.props.id + "filename"} className="col-sm-3 control-label">Dump file</label>
                        <div className="col-sm-9">
                            <input type="file" accept="*.dt"
                                className="form-control"
                                onChange={this.handleFilenameChange.bind(this)}
                                placeholder={"e.g file name with dump " + this.props.basename + ".dt"}/>
                            <progress id={this.props.id + "progress"} max="100" value="0" className="col-sm-12">0% complete</progress>
                        </div>

                        <div className="checkbox col-sm-4 pull-right">
                            <input type='checkbox' 
                                onChange={this.handleSave.bind(this)}
                                checked={this.state.saved}/>
                            <label>Save credentials</label>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}