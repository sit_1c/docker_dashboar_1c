import * as React from 'react'
import { Base, Cluster, ClusterListItem } from './ClusterListItem'

import { NewBaseDialog } from './newBaseModal'
import { DialogTrigger } from './dialogTrigger'

import * as io from 'socket.io-client'

import {serverSocket} from './App'

export class ClusterListProps {
    containerId: string
    cluster?: Cluster
    bases?: Base[]
}

export class ClusterList extends React.Component<{containerId: string}, ClusterListProps> {
    constructor(props : any) {
        super(props)
        
        this.state = {
            containerId: props.containerId,
            cluster: new Cluster,
            bases: []
        }

        serverSocket.on('cluster.info', (clusterInfo: any) => {
            this.setState({
                //containerId: this.state.containerId,
                cluster: {clusterId: clusterInfo.cluster, host: clusterInfo.host, name: clusterInfo.name},
                bases: []
            })

            serverSocket.emit('cluster.getBases', {containerId: props.containerId, clusterId: clusterInfo.cluster})
        })

        serverSocket.on('cluster.bases', (clusterBases: any) => {
            this.setState({
                //containerId: this.state.containerId,
                //cluster: this.state.cluster,
                bases: clusterBases.map(this.mapBase, this)
            })
        })

        serverSocket.on('cluster.downloadDump', (msg: any) => {
            console.log('download url:' + msg.url)
            window.open(msg.url, '_blank')
        })
    }

    componentDidMount() {
        serverSocket.emit('cluster.getInfo', {containerId: this.props.containerId})
    }
    mapBase(base:any): Base {        
        return {
            containerId: this.state.containerId,
            clusterId: this.state.cluster.clusterId,
            baseId: base.infobase,
            name: base.name,
            decsription: base.descr
        }
    }

    onNewBaseButtonClick(baseName:string, description:string) {  
        serverSocket.emit('cluster.createBase', 
            {containerId: this.state.containerId, clusterId: this.state.cluster.clusterId, baseName: baseName, description: description})
    }

    render() {
        return (
            <div>
                <h4>Cluster: {this.state.cluster.name} ({this.state.cluster.host})</h4>
                <a className="btn btn-default" target='_blank' href={ 'http://' + this.context.ServerInfo.dockerHost + ':' + this.context.ServerInfo.noVNCPort }>Open VNC console</a>
                <DialogTrigger id="newBaseModal" buttonText="New base" />
                <NewBaseDialog id="newBaseModal" onAction={this.onNewBaseButtonClick.bind(this)} />

                <p>{ this.state.bases.length == 0 ? "No bases to show" : "" }</p>
                <table className="table table-striped">
                    <th className="col-sm-2">Base name</th>
                    <th className="col-sm-5">Description</th>
                    <th className="col-sm-7">Actions</th>
                    <tbody>
                        { this.state.bases.map(c => <ClusterListItem {...c} />) }
                    </tbody>
                </table>
            </div>
        )
    }
}