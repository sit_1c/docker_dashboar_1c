import * as io from 'socket.io-client'

export const serverSocket = io();

import * as React from 'react'

import { Container, ContainerListItem } from './containerListItem'
import { ContainerList } from './containerList'
import { NewContainerDialog } from './newContainerModal'
import { DialogTrigger } from './dialogTrigger'

import * as _ from 'lodash'

import { TransactionModal } from './TransactionModal'

console.log("Connected");

class AppState {
    containersApp?: Container[]
    containersSQL?: Container[]
    containersOthers?: Container[]
}

export class AppComponent extends React.Component<{}, AppState> {
    constructor(props : any) {
        super(props)
        this.state = {
            containersApp: [],
            containersSQL: [],
            containersOthers: []
        }

        serverSocket.on('containers.list', (containers: any) => {
             let partitionedApp = _.partition(containers, (c: any) => c.Labels.hasOwnProperty('app'))
             let partitionedSQL = _.partition(partitionedApp[1], (c: any) => c.Labels.hasOwnProperty('sql'))

            this.setState({
                containersApp: partitionedApp[0].map(this.mapContainer), 
                containersSQL: partitionedSQL[0].map(this.mapContainer), 
                containersOthers: partitionedSQL[1].map(this.mapContainer), 
            })
        })

        serverSocket.on('server.info', (args: any) => {
            this.context.ServerInfo = {
                dockerHost: args.dockerHost,
                noVNCPort: args.noVNCPort,
                httpPort: args.httpPort
            } 
        })    

        serverSocket.on('image.error', (args: any) => {
            alert(args.message.json.message)
        })    

        serverSocket.on('transaction.start', (args:any) => {
            console.log('transaction.start:' + args)
            this.showTransactionPanel()
        })

        serverSocket.on('transaction.finish', (args:any) => {
            console.log('transaction.finish:' + args)

//            if( args == '0')
//                this.closeTransactionPanel()
        })

        // socket.on('server.log', (args:any) => {
        //     console.log('server.log:' + args)
        //     document.getElementById('logPanel').innerText += args
        // })
    }

    showTransactionPanel() {
        let modalDialog = document.getElementById('TransactionModal')
        $(modalDialog).modal('show')
    }

    closeTransactionPanel() {
        let modalDialog = document.getElementById('TransactionModal')
        $(modalDialog).modal('hide')
    }

    mapContainer(container:any): Container {        
        let containerType = 'other'

        if( container.Labels.hasOwnProperty('app') )
            containerType = 'app'
        
        if( container.Labels.hasOwnProperty('sql') )
            containerType = 'sql'

        return {
            id: container.Id,
            name: _.chain(container.Names)
                .map((n: string) => n.substr(1))
                .join(", ")
                .value(),
            state: container.State,
            status: `${container.State} (${container.Status})`,
            image: container.Image,
            type: containerType
        }
    }

    componentDidMount() {
        serverSocket.emit('containers.list')
    }

    onRunImage(name: String) {
        serverSocket.emit('image.run', { name: name })
    }

    onClearLogClick() {
        document.getElementById('logPanel').innerText = ''
    }

    render() {
        return (
            <div className="container col-md-12">
                <h1 className="page-header">Docker Dashboard</h1>
                <div className="col-md-1"></div>
                <div className="col-md-8">
                    <DialogTrigger id="newContainerModal" buttonText="New container" />
                    <ContainerList title="App containers" containers={this.state.containersApp} />
                    <ContainerList title="SQL containers" containers={this.state.containersSQL} />
                    <ContainerList title="Others containers" containers={this.state.containersOthers} />

                    <NewContainerDialog id="newContainerModal" onRunImage={this.onRunImage.bind(this)} />
                    <TransactionModal id={'transactionModal'} />
                </div>

                {/* <div className="col-md-3 nav navbar-nav pull-right">
                    <button onClick={this.onClearLogClick.bind(this)} className="btn btn-default">
                        <span className="glyphicon glyphicon-trash" aria-hidden="true"></span> Clear</button>
                    <br/>
                    <span id="logPanel"></span>
                </div> */}

            </div>
        )
    }
}

