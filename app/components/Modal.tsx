import * as React from 'react'

import { isUndefined } from 'lodash'

import {serverSocket} from './App'

interface ModalProperties {
    id: string
    title: string
    buttonText?: string
    storeId?: string
    onLoadCredentials?: (modalForm:JQuery<HTMLElement>) => boolean|undefined
    onSaveCredentials?: () => boolean|undefined
    onButtonClicked?: (modalForm:JQuery<HTMLElement>) => boolean|undefined
}

export default class Modal extends React.Component<ModalProperties, {}> {
    // Store the HTML element id of the modal popup
    modalElementId: string

    constructor(props: ModalProperties) {
        super(props)
        this.modalElementId = `#${this.props.id}`
    }

    onPrimaryButtonClick() {
        // Delegate to the generic button handler defined by the inheriting component
        if (this.props.onButtonClicked) {
            if (this.props.onButtonClicked($(this.modalElementId)) !== false) {

                if(this.props.onSaveCredentials) {
                    let divElem = document.getElementById(this.props.id)
                    divElem.dispatchEvent(new CustomEvent('saveCredentails'))
                }

                // Use Bootstrap's jQuery API to hide the popup
                $(this.modalElementId).modal('hide')
            }
        }
    }

    onLoad(ev:any) {
        if(this.props.onLoadCredentials) {
            serverSocket.emit('system.loadCredentails', this.props.storeId)
                .on('system.сredentails', (data:any) => {
                    console.log('result for ' + data)
                    this.props.onLoadCredentials(data);
                })                
        }
    }

    onSave(ev:any) {
        if(this.props.onSaveCredentials) {
            let data = this.props.onSaveCredentials();

            if( !isUndefined(data) )
            serverSocket.emit('system.saveCredentails', this.props.storeId, data)
        }
    }

    componentDidMount() {
        if(this.props.onLoadCredentials) {
            let divElem = document.getElementById(this.props.id)
            divElem.addEventListener('loadCredentails', this.onLoad.bind(this))
            divElem.addEventListener('saveCredentails', this.onSave.bind(this))

            $(divElem).on("shown.bs.modal", function (e) {
                e.target.dispatchEvent(new CustomEvent('loadCredentails'))
            })

            // $(divElem).on("hidden.bs.modal", function (e) {
            //     e.target.dispatchEvent(new CustomEvent('saveCredentails'))
            // })
        }
    }

    render() {
        return (
            <div className="modal fade" id={ this.props.id }>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 className="modal-title">{ this.props.title }</h4>
                        </div>
                        <div className="modal-body">
                            { this.props.children }
                        </div>
                        <div className="modal-footer">
                            {(this.props.buttonText ?this.renderSubmitButton() :'')}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderSubmitButton() {
        return (
            <button type="button"
                onClick={this.onPrimaryButtonClick.bind(this)}
                className="btn btn-primary">{ this.props.buttonText || "Ok" }
            </button>
        )
    }
}