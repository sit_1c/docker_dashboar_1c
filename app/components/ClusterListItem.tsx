import * as React from 'react'

import * as io from 'socket.io-client'
import { DeleteBaseModal } from './DeleteBaseModal'
import { ExportBaseModal } from './ExportBaseModal'
import { ImportBaseModal } from './ImportBaseModal'
import { UpdateBaseModal } from './UpdateBaseModal'

//const socket = io.connect()
import {serverSocket} from './App'

export class Cluster {
    clusterId: string
    host: string
    name: string
}

export class Base {
    containerId: string
    clusterId: string
    baseId: string
    name: string
    decsription: string
}

export class ClusterListItem extends React.Component<Base, {}> {
    
    onExportClick(username:string, password:string) {  
        serverSocket.emit('cluster.exportBase',  
            {containerId: this.props.containerId, clusterId: this.props.clusterId, baseName:this.props.name, username:username, password:password })
    }

    onImportClick(username:string, password:string) {  
        serverSocket.emit('cluster.importBase',  
             {containerId: this.props.containerId, baseName:this.props.name, clusterId: this.props.clusterId, username:username, password:password })
    }

    onUpdateClick(repo:string, branch:string, repo_username: string, repo_password: string, username: string, password: string) {  
        console.log(repo, branch, repo_username, repo_password, username, password)
        serverSocket.emit('cluster.updateBase',  
             {containerId: this.props.containerId, baseName:this.props.name, clusterId: this.props.clusterId, 
                repo: repo, branch:branch,
                repo_username: repo_username, repo_password: repo_password,
                username: username, password: password})
    }

    onDeleteClick(username:string, password:string) {  
        serverSocket.emit('cluster.dropBase',  
            {containerId: this.props.containerId, clusterId: this.props.clusterId, baseId: this.props.baseId, username:username, password:password })
    }

    render() {
        const href = this.props.name

        return (
            <tr>
                <td><h4>{this.props.name}</h4></td>
                <td>{this.props.decsription}</td>
                <td>
                    <div className="btn-toolbar">
                        <a href={ '#' + href + '_import' } className="btn btn-default" data-toggle="modal" title="Import infobase">
                            <span className="glyphicon glyphicon-floppy-open" aria-hidden="true"></span></a>

                        <a href={ '#' + href + '_export' } className="btn btn-default" data-toggle="modal" title="Export infobase">
                            <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span></a>

                        <a href={ '#' + href + '_update' } className="btn btn-default" data-toggle="modal" title="Update infobase">
                            <span className="glyphicon glyphicon-retweet" aria-hidden="true"></span></a>

                        <a href={ '#' + href + '_delete' } className="btn btn-default" data-toggle="modal" title="Delete infobase">
                            <span className="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></a>
                    </div>
                </td>

                <ImportBaseModal id={ href + '_import' } basename={this.props.name} onAction={this.onImportClick.bind(this)} />
                <ExportBaseModal id={ href + '_export' } basename={this.props.name} onAction={this.onExportClick.bind(this)} />
                <UpdateBaseModal id={ href + '_update' } basename={this.props.name} onAction={this.onUpdateClick.bind(this)} />
                <DeleteBaseModal id={ href + '_delete' } basename={this.props.name} onAction={this.onDeleteClick.bind(this)} />            
            </tr>
        )}
}