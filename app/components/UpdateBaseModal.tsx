import * as React from 'react'
import Modal from './modal'
import * as classNames from 'classnames'

interface ModalProperties {
    id: string
    basename: string
    onAction?: (repo:string, branch:string, repo_username: string, repo_password: string, username: string, password: string) => void
}

interface ModalState {
    file: File
    repo: string
    branch: string
    repo_username: string
    repo_password: string
    username: string
    password: string
    saved: boolean
}

const SLICE_SIZE = 1000 * 1024

export class UpdateBaseModal extends React.Component<ModalProperties, ModalState> {
    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            file: null,
            repo: '',
            branch: '',
            repo_username: '',
            repo_password: '',
            username: '',
            password: '',
            saved: false
        }
    }

    doAction(modalForm:JQuery<HTMLElement>) {
        if( this.state.file ) {
            var xhr = new XMLHttpRequest;
                        
            xhr.open('PUT', `http://${this.context.ServerInfo.dockerHost}:${this.context.ServerInfo.httpPort}/tmp/${this.props.basename}.cf`, true)
            xhr.setRequestHeader('crossDomain', 'true')
            xhr.overrideMimeType('application/octet-stream');

            // Listen to the upload progress.
            var progressBar = document.getElementById(this.props.id + "progress") as HTMLProgressElement;
            xhr.upload.addEventListener('progress', (evn:ProgressEvent<XMLHttpRequestUpload>) => {
                //console.log('upload progress ' + e.loaded  + ' of ' + e.total)
                if (evn.lengthComputable) {
                    progressBar.value = (evn.loaded / evn.total) * 100;
                    //progressBar.textContent = progressBar.value.toString(); // Fallback for unsupported browsers.
                }
            });

            xhr.upload.addEventListener('loadend', (evn:any) => {
                console.log('uploading completed')
                this.props.onAction('', '', '', '', this.state.username, this.state.password)

                //Close manuale
                let divElem = document.getElementById(this.props.id)
                divElem.dispatchEvent(new CustomEvent('saveCredentails'))
                
                modalForm.modal('hide')
            })

            xhr.upload.addEventListener('error', (evn:any) => {
                console.log('error:' + evn)
            })

            xhr.upload.addEventListener('abort', (evn:any) => {
                console.log('aborted')
            })

            xhr.send(this.state.file)

            return false
        } else {
            this.props.onAction(this.state.repo, this.state.branch, this.state.repo_username, this.state.repo_password, this.state.username, this.state.password)
        
            return true
        }
    }

    onLoadCredentials(e:any) {
        console.log('onLoadCredentials called', e)
        
        this.setState({
            username: (e.username ?e.username :this.state.username),
            password: (e.username ?e.password :this.state.password),
            repo_username: (e.repo_username ?e.repo_username :this.state.repo_username),
            repo_password: (e.repo_password ?e.repo_password :this.state.repo_password),
            repo: (e.repo ?e.repo :this.state.repo),
            branch: (e.branch ?e.branch :this.state.branch),
            saved: (e.saved ?e.saved :false)
        })
    }

    onSaveCredentials(e:any) {
        console.log('onSaveCredentials called')
        
        return (this.state.saved ?this.state :undefined)
    }

    handleUsernameChange(e:any) {
        this.setState({
            username: e.target.value
        });
    }

    handlePasswordChange(e:any) {
        this.setState({
            password: e.target.value
        });
    }

    handleRepoUsernameChange(e:any) {
        this.setState({
            repo_username: e.target.value
        });
    }

    handleRepoPasswordChange(e:any) {
        this.setState({
            repo_password: e.target.value
        });
    }

    handleRepoChange(e:any) {
        this.setState({
            repo: e.target.value
        });
    }

    handleBranchChange(e:any) {
        this.setState({
            branch: e.target.value
        });
    }

    handleFilenameChange(e:any) {
        this.setState({
            file: e.target.files[0]
        });
    }

    handleSave(e:any) {
        this.setState({
          saved: e.target.checked
        });
    }

    render() {
        let inputClass = classNames({
            "form-group": true,
            "has-error": false
        })
        
        return (
            <Modal id={this.props.id} buttonText="Logon" title="Login information for infobase" 
                    onButtonClicked={this.doAction.bind(this)}
                    storeId={this.props.basename}
                    onLoadCredentials={this.onLoadCredentials.bind(this)}
                    onSaveCredentials={this.onSaveCredentials.bind(this)}>
                <form className="form-horizontal">
                    <div className={inputClass}>
                        <label htmlFor={this.props.id + "username"} className="col-sm-3 control-label">User name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                id={this.props.id + "username"}
                                value={this.state.username}
                                onChange={this.handleUsernameChange.bind(this)}
                                placeholder="e.g Administrator"/>
                        </div>
                        
                        <label htmlFor={this.props.id + "password"} className="col-sm-3 control-label">User password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                className="form-control"
                                id={this.props.id + "password"}
                                value={this.state.password}
                                onChange={this.handlePasswordChange.bind(this)}
                                placeholder="e.g password for Administrator"/>
                        </div>

                        <label className="col-sm-12 control-label">update from git repo</label>

                        <label htmlFor={this.props.id + "repo"} className="col-sm-3 control-label">Repo path</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                id={this.props.id + "repo"}
                                value={this.state.repo}
                                onChange={this.handleRepoChange.bind(this)}
                                placeholder="e.g http://gitlab.com/.../repo.git"/>
                        </div>

                        <label htmlFor={this.props.id + "branch"} className="col-sm-3 control-label">Branch</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                id={this.props.id + "branch"} 
                                value={this.state.branch}
                                onChange={this.handleBranchChange.bind(this)}
                                placeholder="e.g master"/>
                        </div>

                        <label htmlFor={this.props.id + "repo_username"} className="col-sm-3 control-label">Repo user name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                id={this.props.id + "repo_username"}
                                value={this.state.repo_username}
                                onChange={this.handleRepoUsernameChange.bind(this)}
                                placeholder="e.g Administrator@gitlab.com"/>
                        </div>
                        
                        <label htmlFor={this.props.id + "repo_password"} className="col-sm-3 control-label">Repo user password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                className="form-control"
                                id={this.props.id + "repo_password"}
                                value={this.state.repo_password}
                                onChange={this.handleRepoPasswordChange.bind(this)}
                                placeholder="e.g password for Administrator@gitlab.com"/>
                        </div>

                        <label className="col-sm-12 control-label">update from file</label>

                        <label htmlFor={this.props.id + "filename"} className="col-sm-3 control-label">Dump cfg</label>
                        <div className="col-sm-9">
                            <input type="file" accept="*.cf"
                                className="form-control"
                                id={this.props.id + "filename"}
                                onChange={this.handleFilenameChange.bind(this)}
                                placeholder={"e.g file name with dump " + this.props.basename + ".cf"}/>
                            <progress id={this.props.id + "progress"} max="100" value="0" className="col-sm-12">0% complete</progress>
                        </div>

                        <div className="checkbox col-sm-4 pull-right">
                            <input type='checkbox' 
                                onChange={this.handleSave.bind(this)}
                                checked={this.state.saved}/>
                            <label>Save credentials</label>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}