import * as React from 'react'
import Modal from './modal'
import * as classNames from 'classnames'

interface ModalProperties {
    id: string,
    onAction?: (name: string, description: string) => void
}

interface ModalState {
    baseName: string
    description: string
    isValid: boolean
}

export class NewBaseDialog extends React.Component<ModalProperties, ModalState> {

    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            baseName: '',
            description: '',
            isValid: false
        }
    }

    onBaseNameChange(e: any) {
        const name = e.target.value
    
        this.setState({
            baseName: name,
            isValid: name.length > 0
        })
    }

    onDescriptionChange(e: any) {
        const description = e.target.value
    
        this.setState({
            description: description
        })
    }

    createBase() {
        if (this.state.isValid && this.props.onAction)
            this.props.onAction(this.state.baseName, this.state.description)
    
        return this.state.isValid
    }

    render() {
        let inputClass = classNames({
            "form-group": true,
            "has-error": !this.state.isValid
        })
        
        return (
            <Modal id="newBaseModal" buttonText="Run" title="Create a new infobase" onButtonClicked={this.createBase.bind(this)}>
                <form className="form-horizontal">
                    <div className={inputClass}>
                        <label htmlFor="baseName" className="col-sm-3 control-label">Base name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                onChange={this.onBaseNameChange.bind(this)}
                                id="baseName"
                                placeholder="e.g test"/>
                        </div>
                        
                        <label htmlFor="descritpion" className="col-sm-3 control-label">Description</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                onChange={this.onDescriptionChange.bind(this)}
                                id="descritpion"
                                placeholder="e.g base for testing"/>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}