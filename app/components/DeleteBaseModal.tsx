import * as React from 'react'
import Modal from './modal'
import * as classNames from 'classnames'

interface ModalProperties {
    id: string
    basename: string
    onAction?: (username: string, password: string) => void
}

interface ModalState {
    username: string
    password: string
}

export class DeleteBaseModal extends React.Component<ModalProperties, ModalState> {

    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            username: '',
            password: ''
        }
    }

    doAction() {
        if (this.props.onAction)
            this.props.onAction(this.state.username, this.state.password)
    }

    handleUsernameChange(e:any) {
        this.setState({
          username: e.target.value
        });
    }

    handlePasswordChange(e:any) {
        this.setState({
          password: e.target.value
        });
    }

    render() {
        let inputClass = classNames({
            "form-group": true,
            "has-error": false
        })
        
        return (
            <Modal id={this.props.id} buttonText="Logon" title="Login information for infobase" onButtonClicked={this.doAction.bind(this)}>
                <form className="form-horizontal">
                    <h2>Confirm delete base {this.props.basename}...</h2>
                    <h3>All data will be lost!!!</h3>
                    <div className={inputClass}>
                        <label htmlFor={this.props.id + "username"} className="col-sm-3 control-label">User name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                id={this.props.id + "username"}
                                onChange={this.handleUsernameChange.bind(this)}
                                placeholder="e.g Administrator"/>
                        </div>
                        
                        <label htmlFor={this.props.id + "password"} className="col-sm-3 control-label">User password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                className="form-control"
                                id={this.props.id + "password"}
                                onChange={this.handlePasswordChange.bind(this)}
                                placeholder="e.g password for Administrator"/>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}