import * as React from 'react'
import Modal from './modal'
import * as classNames from 'classnames'

interface ModalProperties {
    id: string
    basename: string
    onAction?: (username: string, password: string) => void
}

interface ModalState {
    username: string
    password: string
    saved: boolean
}

export class ExportBaseModal extends React.Component<ModalProperties, ModalState> {
    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            username: '',
            password: '',
            saved: false
        }
    }

    onLoadCredentials(e:any) {
        console.log('onLoadCredentials called', e)
        
        this.setState({
            username: (e.username ?e.username :this.state.username),
            password: (e.username ?e.password :this.state.password),
            saved: (e.saved ?e.saved :false)
        })
    }

    onSaveCredentials(e:any) {
        console.log('onSaveCredentials called')
        
        return (this.state.saved ?this.state :undefined)
    }

    doAction() {
        if (this.props.onAction)
            this.props.onAction(this.state.username, this.state.password)
    }

    handleUsernameChange(e:any) {
        this.setState({
          username: e.target.value
        });
    }

    handlePasswordChange(e:any) {
        this.setState({
          password: e.target.value
        });
    }

    handleSave(e:any) {
        this.setState({
          saved: e.target.checked
        });
    }

    render() {
        let inputClass = classNames({
            "form-group": true,
            "has-error": false
        })
        
        return (
            <Modal id={this.props.id} buttonText="Logon" title="Login information for infobase" 
                    onButtonClicked={this.doAction.bind(this)} 
                    storeId={this.props.basename}
                    onLoadCredentials={this.onLoadCredentials.bind(this)}
                    onSaveCredentials={this.onSaveCredentials.bind(this)}>
                <form className="form-horizontal">
                    <div className={inputClass}>
                        <label htmlFor={this.props.id + "username"} className="col-sm-3 control-label">User name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                className="form-control"
                                placeholder="e.g Administrator"
                                onChange={this.handleUsernameChange.bind(this)}
                                value={this.state.username}/>
                        </div>
                        
                        <label htmlFor={this.props.id + "password"} className="col-sm-3 control-label">User password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                className="form-control"
                                placeholder="e.g password for Administrator"
                                onChange={this.handlePasswordChange.bind(this)}
                                value={this.state.password}/>
                        </div>
                        
                        <div className="checkbox col-sm-4 pull-right">
                            <input type='checkbox' 
                                onChange={this.handleSave.bind(this)}
                                checked={this.state.saved}/>
                            <label>Save credentials</label>
                        </div>
                    </div>
                </form>
            </Modal>
        )
    }
}