import * as React from 'react'
import Modal from './modal'

import ScrollArea from 'react-scrollbars-custom'

//let socket = io.connect()
import {serverSocket} from './App'

interface ModalProperties {
    id: string,
}

interface ModalState {
    isOpen: boolean,
}

export class TransactionModal extends React.Component<ModalProperties, ModalState> {
    constructor(props: ModalProperties) {
        super(props)

        this.state = {
            isOpen: false
        }

        serverSocket.on('server.log', (args:any) => {
            console.log('server.log:' + args)
            document.getElementById('logPanel').innerText += args
        })

        serverSocket.on('transaction.processing', (args:any) => {
            if( !this.state.isOpen ) {    
                let divElem = document.getElementById('TransactionModal')            
                $(divElem).modal('show')
                console.log('reopen modal with:' + args)
                document.getElementById('logPanel').innerText = args
            }
        })
    }

    onOpenState(ev:any) {
        this.setState({
            isOpen: true
        })    
    }

    onCloseState(ev:any) {
        this.setState({
            isOpen: false
        })    
    }

    componentDidMount() {
        let divElem = document.getElementById('TransactionModal')
        divElem.addEventListener('onShow', this.onOpenState.bind(this))
        divElem.addEventListener('onHide', this.onCloseState.bind(this))

        $(divElem).on("shown.bs.modal", function (e) {
            e.target.dispatchEvent(new CustomEvent('onShow'))
        })

        $(divElem).on("hidden.bs.modal", function (e) {
            e.target.dispatchEvent(new CustomEvent('onHide'))
        })
    }    
    render() {
        return (
            <Modal id="TransactionModal" title="Transaction in progress..">
                    <ScrollArea style={{ height: 450 }}>
                        <div className="col-sm-9" id="logPanel"/>
                    </ScrollArea>
            </Modal>
        )
    }
}