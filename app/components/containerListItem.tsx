import * as React from 'react'
import * as classNames from 'classnames'

import { ClusterList } from './ClusterList'

import * as io from 'socket.io-client'

//const socket = io.connect()
import {serverSocket} from './App'

export interface Container {
    id: string
    name: string
    image: string
    state: string
    status: string

    type: string
}

export class ContainerListItem extends React.Component<Container, {}> {
    // Helper method for determining whether the container is running or not
    isRunning() {
        return this.props.state === 'running'
    }

    onActionButtonClick() {  
        const evt = this.isRunning() ? 'container.stop' : 'container.start'
        serverSocket.emit(evt, { id: this.props.id })
    }

    render() {
        const panelClass = this.isRunning() ? 'success' : 'default'
        const classes = classNames('panel', `panel-${panelClass}`)
        const containerAction = this.isRunning() ? 'Stop' : 'Start'

        if( this.props.type == 'app' && this.isRunning())
            return (
                <div className="col-sm-8">
                    <div className={ classes }>
                        <div className="panel-heading">{ this.props.name + ' (' + this.props.type + ')' }</div>
                        <div className="panel-body">
                            Status: {this.props.status}<br/>
                            Image: {this.props.image}<br/>
                            <ClusterList containerId={this.props.id}/>
                        </div>
                        <div className="panel-footer">                            
                            <button onClick={this.onActionButtonClick.bind(this)}
                                className="btn btn-default">{containerAction}</button>
                        </div>
                    </div>
                </div>)
        else
            return (
                <div className="col-sm-4">
                    <div className={ classes }>
                        <div className="panel-heading">{ this.props.name + ' (' + this.props.type + ')' }</div>
                        <div className="panel-body">
                            Status: {this.props.status}<br/>
                            Image: {this.props.image}<br/>
                        </div>
                        <div className="panel-footer">
                            <button onClick={this.onActionButtonClick.bind(this)}
                                className="btn btn-default">{containerAction}</button>
                        </div>
                    </div>
                </div>)
    }
}