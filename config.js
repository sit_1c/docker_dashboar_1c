require('dotenv').config()

function defaultValue(v, dv) {
    let result = ((process.env[v] != undefined) ?process.env[v] :dv);

    if(result == undefined)
        throw new Error(`Env var ${v} not defined`);

    console.log(`Env var ${v} ${result}`);

    return result;
}

const config = {
    dockerHost: defaultValue('dockerHost'),
    dockerPort: defaultValue('dockerPort', '2375'),
    dbHost: defaultValue('dbHost'),
    noVNCPort: defaultValue('noVNCPort', '6080'),
    httpPort: defaultValue('httpPort', '8000'),

    racpath: '/opt/1C/v8.3/x86_64/rac',
    raspath: '/opt/1C/v8.3/x86_64/ras',
    clipath: '/opt/1C/v8.3/x86_64/1cv8'
}

module.exports = {config}