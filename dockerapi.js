const {config} = require('./config')

let Docker = require("dockerode");
let isWindows = process.platform === "win32";

let options = {};

if (isWindows) {
    options = {
        host: config.dockerHost,
        port: config.dockerPort
    }
} else {
    options = {
        socketPath: '/var/run/docker.sock'
    }
}

module.exports = new Docker(options);