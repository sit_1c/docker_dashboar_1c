let express = require('express')
let path = require('path')
let app = express()
let server = require('http').Server(app)

var fs = require('fs');

let io = require('socket.io')(server)

const { split, random, result, replace } = require('lodash')

// Use the environment port if available, or default to 3000
let port = process.env.PORT || 3000

const {config} = require('./config')

let docker = require('./dockerapi')

// Serve static files from /public
app.use(express.static('public'))

// Create an endpoint which just returns the index.html page
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'index.html')))

// Start the server
server.listen(port, () => console.log(`Server started on port ${port}`))

function refreshContainers(socket) {
    clientAddress = socket.handshake.address

    docker.listContainers({ all: true}, (err, containers) => {
        if( err )
            console.error(err.message)

        io.in(clientAddress).emit('containers.list', containers)
    })
}

function containerExecCommandList(socket, containerId, command, callback) {
    let clientAddress = socket.handshake.address

    console.log(command)
    //io.in(clientAddress).emit('server.log', `-->${command}\n`)

    let container = docker.getContainer(containerId)

    container.exec({Cmd: ['bash', '-c', command], AttachStdin: true, AttachStdout: true, AttachStderr: true}, function(err, exec) {
        if (err) {
            console.log(`The container has ${container.id} error:\n\n${err}`)
            io.in(clientAddress).emit('server.log', `The container has ${container.id} error:\n\n${err}`)
        }

        exec.start({stdout: true, stderr: true}, function(err, stream) {
            if (err) {
                console.log(`The container has ${container.id} error:\n\n${err}`)
                io.in(clientAddress).emit('server.log', `The container has ${container.id} error:\n\n${err}`)
            }

            console.log(`Reattached to container: ${container.id}`)
    
            var refreshIntervalId = setInterval(() => {
                io.in(clientAddress).emit('transaction.processing', outputData)
            }, 5000)

            //stream.setEncoding('utf8');
            //stream.pipe(process.console);                

            let outputData = ''

            stream.on('error', (error) => {
                //console.log('-- ERROR STREAM --\n', data.toString())
                outputData = outputData + error.toString()
                io.in(clientAddress).emit('server.log', error.toString())
            });

            stream.on('data', (data) => {
                //console.log('-- STREAM --\n', data.toString())
                outputData = outputData + data.toString()
                io.in(clientAddress).emit('server.log', data.toString())
            });

            stream.on('end', () => {
                let result = outputData.trim()
                callback(outputData, result.charAt(result.length - 1))

                clearInterval(refreshIntervalId)
                //io.emit('server.log', '-- STREAM END --')
            });             
        });
      });
}

function clusterGetInfo(socket, containerId) {
    let clientAddress = socket.handshake.address

    let command = config.racpath + ' cluster list'

    containerExecCommandList(socket, containerId, command, (data) => {
        clusterInfo = {}
        
        clusterInfoData = data.substring(data.indexOf('cluster'))

        split(clusterInfoData, '\n').forEach(element => {
            kv = element.split(':');

            if(kv[0] && kv[0].trim())
                clusterInfo[kv[0].trim()] = (kv[1] ?kv[1].trim() :'')
        });

        console.log(clusterInfo)

        io.in(clientAddress).emit('cluster.info', clusterInfo)
    })
}

function clusterGetBases(socket, containerId, clusterId) {
    let clientAddress = socket.handshake.address

    let command = config.racpath + ` infobase summary list --cluster ${clusterId}`

    console.log(command)

    containerExecCommandList(socket, containerId, command, (data) => {
        //console.log(data)

        index = 0
        bases = [{}]
        
        basesInfoData = data.substring(data.indexOf('infobase'))

        split(basesInfoData, '\n').forEach(element => {
            kv = element.split(':');

            if(!kv[0]) {
                ++index
            } else {
                if(!bases[index])
                    bases[index] = {}

                bases[index][kv[0].trim()] = (kv[1] ?kv[1].trim() :'')
            }
        });

        console.log(bases)

        io.in(clientAddress).emit('cluster.bases', bases)
    })

}

//setInterval(refreshContainers, 2000)

io.on('connection', socket => {
    let clientAddress = socket.handshake.address

    console.log('join to ' + clientAddress);
    socket.join(clientAddress);

    io.emit('server.info', 
        { 
            dockerHost: config.dockerHost,
            dockerPort: config.dockerPort,
            dbHost: config.dbHost,
            noVNCPort: config.noVNCPort,
            httpPort: config.httpPort
        }        
    )

    socket.on('containers.list', () => {
        refreshContainers(socket)
    })

    socket.on('container.start', args => {
        const container = docker.getContainer(args.id)
    
        if (container) {
            container.start((err, data) => {
                if( err )
                    console.error(err.message)
    
                refreshContainers(socket)
            })
        }
    })
    
    socket.on('container.stop', args => {
        const container = docker.getContainer(args.id)
    
        if (container) {
            container.stop((err, data) => refreshContainers(socket))
        }
    })

    socket.on('image.run', args => {
        let clientAddress = socket.handshake.address
        
        docker.createContainer({ Image: args.name }, (err, container) => {
            if (!err)
                container.start((err, data) => {
                    if (err)
                        io.in(clientAddress).emit('image.error', { message: err })
                })
            else
                io.in(clientAddress).emit('image.error', { message: err })
        })
    })

    socket.on('system.loadCredentails', (storeId) => {
        let clientAddress = socket.handshake.address

        console.log('system.loadCredentails(' + storeId + ')')

        fs.readFile('./../store/credentails_' + storeId + '.json', function (err, data) {
            if (err) {
                console.log('There has been an error loading your configuration data.');
                console.log(err.message);
            } else {
                try{
                    io.in(clientAddress).emit('system.сredentails', JSON.parse(data.toString()))
                    console.log('Configuration load successfully.')
                } catch (e) {
                    console.log('Configuration load failed. ' + e.message)
                }
            }
        });    
    })

    socket.on('system.saveCredentails', (storeId, storeData) => {
        console.log('system.saveCredentails(' + storeId + ')')

        let original = JSON

        try{
            var original_data = fs.readFileSync('./../store/credentails_' + storeId + '.json')
            if (original_data) {
                original = JSON.parse(original_data)
            }
        } catch(e) {
            console.log('Configuration load failed. ' + e.message)
        }

        var data = Object.assign({}, original, storeData);
        var dataJSON = JSON.stringify(data)

        fs.writeFile('./../store/credentails_' + storeId + '.json', dataJSON, function (err) {
            if (err) {
                console.log('There has been an error saving your configuration data.');
                console.log(err.message);
            } else {
                console.log('Configuration saved successfully.')
            }
        });    
    })
    
    socket.on('cluster.getInfo', args => {
        console.log('cluster.getInfo(' + args.containerId + ')')
        clusterGetInfo(socket, args.containerId)
    })    

    socket.on('cluster.getBases', args => {
        console.log('cluster.getBases(' + args.clusterId + ')')
        clusterGetBases(socket, args.containerId, args.clusterId)
    })    

    socket.on('cluster.createBase', args => {
        let clientAddress = socket.handshake.address

        msg = 'cluster.createBase(' + args.containerId, args.clusterId, args.baseName, args.description + ')'

        io.in(clientAddress).emit('transaction.start', msg)

        console.log(msg)

        let command = config.racpath + 
            ` infobase --cluster=${args.clusterId} create --create-database --name=${args.baseName}` +
                ` --dbms=PostgreSQL --db-server=${config.dbHost} --db-user=postgres --db-pwd=postgres --db-name=${args.baseName}` +
                ` --locale=ru --license-distribution=allow --descr='${args.description}'`

        containerExecCommandList(socket, args.containerId, command, (data, result) => {
            console.log(data)

            clusterGetBases(socket, args.containerId, args.clusterId)

            io.in(clientAddress).emit('transaction.finish', '0')
        })
    })    

    socket.on('cluster.dropBase', args => {
        let clientAddress = socket.handshake.address

        msg = 'cluster.dropBase(' + args.containerId, args.clusterId, args.baseId, args.username, args.password + ')'

        io.in(clientAddress).emit('transaction.start', msg)

        console.log(msg)

        let command = config.racpath + 
            ` infobase --cluster=${args.clusterId} drop --infobase=${args.baseId} --drop-database --infobase-user="${args.username}" --infobase-pwd="${args.password}"`

        containerExecCommandList(socket, args.containerId, command, (data, result) => {
            console.log(data)

            clusterGetBases(socket, args.containerId, args.clusterId)

            io.in(clientAddress).emit('transaction.finish', '0')
        })
    })    

    socket.on('cluster.exportBase', args => {
        let clientAddress = socket.handshake.address

        msg = 'cluster.exportBase(' + args.containerId, args.clusterId, args.baseName, args.username, args.password + ')'

        io.in(clientAddress).emit('transaction.start', msg)

        console.log(msg)

        let command = `xvfb-run \
            ${config.clipath} DESIGNER /DumpIB /tmp/${args.baseName}.dt /S localhost/${args.baseName} \
                /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result `
                if( args.username )
                    command += `/N"${args.username}" ` 
                
                if( args.password )
                    command += `/P"${args.password}" `
                
                command += `& pid=$! wait $pid &&
                cat /tmp/${args.baseName}.log &&
                cat /tmp/${args.baseName}.result
                `

        containerExecCommandList(socket, args.containerId, command, (data, result) => {
            console.log('result:', data, result)

            var cmdResult = result.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '')

            if( cmdResult == 0) {
                console.log('download link:' + `http://${config.dockerHost}:${config.httpPort}/tmp/${args.baseName}.dt`)
                io.in(clientAddress).emit('cluster.downloadDump', { url: `http://${config.dockerHost}:${config.httpPort}/tmp/${args.baseName}.dt` })
            }

            io.in(clientAddress).emit('transaction.finish', cmdResult)
        })               
    })    

    socket.on('cluster.importBase', args => {
        let clientAddress = socket.handshake.address

        msg = 'cluster.importBase(' + args.containerId, args.clusterId, args.baseName, args.username, args.password + ')'

        io.in(clientAddress).emit('transaction.start', msg)

        console.log(msg)

        let command = `xvfb-run \
                        ${config.clipath} DESIGNER /RestoreIB /tmp/${args.baseName}.dt /S localhost/${args.baseName} \
                            /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result `
                    if( args.username )
                        command += `/N"${args.username}" ` 
                    
                    if( args.password )
                        command += `/P"${args.password}" `
                                    
                    command +=`& pid=$! wait $pid &&
                        cat /tmp/${args.baseName}.log &&
                        cat /tmp/${args.baseName}.result
                        `
        
        containerExecCommandList(socket, args.containerId, command, (data, result) => {
            console.log('result:', data, result)

            var cmdResult = result.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '')

            io.in(clientAddress).emit('transaction.finish', cmdResult)
        })
    })    

    socket.on('cluster.updateBase', args => {
        let clientAddress = socket.handshake.address

        msg = 'cluster.updateBase(' + args.containerId, args.clusterId, 
        args.baseName, args.repo, args.branch, args.repo_username, args.repo_password, args.username, args.password + ')'

        io.in(clientAddress).emit('transaction.start', msg)

        console.log(msg)

        let command = ''
        if( args.repo ) {
            //update cgf from git repo

            var urlPattern = /(http|ftp|https):\/\/([\w-]+\.[\w-]+[\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/
            var urlElement = args.repo.split(urlPattern)

            let giturl = urlElement[1] + `://${encodeURIComponent(args.repo_username)}:${encodeURIComponent(args.repo_password)}@` + urlElement[2] 

            let repoPath = args.baseName

            //https://github.com/oscript-library/deployka/blob/master/src/%D0%9A%D0%BB%D0%B0%D1%81%D1%81%D1%8B/%D0%9A%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B0%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%D0%A1%D0%B5%D0%B0%D0%BD%D1%81%D0%B0%D0%BC%D0%B8.os
            //УстановитьСтатусБлокировкиСеансов
            //infobase update --infobase=""%3""%4 --cluster=""%1""%2 --sessions-deny=%5 --denied-message=""%6"" --denied-from=""%8"" --permission-code=""%7"""  

            //УстановитьСтатусБлокировкиРЗ
            //infobase update --infobase=""%3""%4 --cluster=""%1""%2 --scheduled-jobs-deny=%5

            //ПолучитьСписокСеансов
            //session list --cluster=""%1""%2 --infobase=""%3""

            //ОтключитьСеанс
            //session terminate --cluster=""%1""%2 --session=""%3""

            //ПолучитьСписокРабочихПроцессов
            //process list --cluster=""%1""%2

            //ПолучитьСоединенияРабочегоПроцесса    
            //connection list --cluster=""%1""%2 --infobase=%3%4 --process=%5

            //РазорватьСоединениеСПроцессом
            //connection disconnect --cluster=""%1""%2 --infobase=%3%4 --process=%5 --connection=%6

            command = `if [ -d "/tmp/${repoPath}" ] 
                then
                    cd /tmp/${repoPath} &&
                    echo "Клонирование репозитария.." &&
                    git checkout "${args.branch}" &&
                    git pull "${giturl}" "${args.branch}" --rebase=true

                else
                    mkdir /tmp/${repoPath} && 
                    echo "Клонирование репозитария.." &&
                    git clone "${giturl}" /tmp/${args.baseName} &&
                    cd /tmp/${repoPath} &&
                    git checkout "${args.branch}"
                fi && 
                rm -rf /tmp/ib${args.baseName} && 
                mkdir /tmp/ib${args.baseName} && 
                echo "Создание локальной базы.." &&
                xvfb-run ${config.clipath} CREATEINFOBASE File=/tmp/ib${args.baseName} \
                    /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result && 
                cat /tmp/${args.baseName}.log && 
                cat /tmp/${args.baseName}.result && 
                echo "Загрузка данных их репозитария.." &&
                xvfb-run ${config.clipath} DESIGNER /F /tmp/ib${args.baseName} /LoadConfigFromFiles "/tmp/${args.baseName}/src" \
                    /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result && 
                cat /tmp/${args.baseName}.log && 
                cat /tmp/${args.baseName}.result && 
                echo "Создание файла конфигурации.." &&
                xvfb-run ${config.clipath} DESIGNER /F /tmp/ib${args.baseName} /DumpCfg "/tmp/${args.baseName}.cf" \
                    /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result &&
                cat /tmp/${args.baseName}.log &&
                cat /tmp/${args.baseName}.result && `
        }

        command = command +
            `echo "Обновление конфигурации базы.." &&
            xvfb-run ${config.clipath} DESIGNER /S localhost/${args.baseName} /LoadCfg "/tmp/${args.baseName}.cf" /UpdateDBCfg \
                /Out /tmp/${args.baseName}.log  /DumpResult /tmp/${args.baseName}.result /N"${args.username}" /P"${args.password}" & pid=$! wait $pid &&
            cat /tmp/${args.baseName}.log &&
            cat /tmp/${args.baseName}.result
            `

        containerExecCommandList(socket, args.containerId, command, (data, result) => {
            console.log('result:', data, result)

            var cmdResult = result.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '')

            io.in(clientAddress).emit('transaction.finish', cmdResult)
        })
    })    
})
